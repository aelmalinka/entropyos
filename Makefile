ARCH ?= riscv64
TARGET = $(ARCH)-unknown-elf

BUILD ?= $(SRCDIR)build

PROJECTS := libk libc test

default: $(PROJECTS)

CWARNINGS = -Wall -Wextra -Wpedantic \
	-Wshadow -Wcast-align -Wwrite-strings -Winline \
	-Wswitch-default -Wfloat-equal \
	-Wmissing-declarations \
	-Wtrampolines \
	-Werror

CXXWARNINGS = -Wall -Wextra -Wpedantic \
	-Wshadow -Wcast-align -Wwrite-strings -Winline \
	-Wswitch-default -Wfloat-equal \
	-Wmissing-declarations \
	-Wzero-as-null-pointer-constant -Wuseless-cast \
	-Wnoexcept -Wnon-virtual-dtor -Wold-style-cast -Woverloaded-virtual \
	-Wtrampolines \
	-Werror

# 2020-02-11 AMR TODO: this kind of sucks
# 2020-02-11 AMR NOTE: is there a way to ?= and override default?
# 2020-02-10 AMR TODO: bump to 2020 when supported?
CFLAGS = -g3 -Og
CFLAGS := $(CFLAGS) -std=c11 $(CWARNINGS)
CXXFLAGS = -g3 -Og
CXXFLAGS := $(CXXFLAGS) -std=c++17 $(CXXWARNINGS)

ifeq ($(DEBUG), 1)
	CFLAGS += -DDEBUG
	CXXFLAGS += -DDEBUG
endif

# 2020-02-11 AMR TODO: this kind of sucks
# 2020-02-11 AMR NOTE: is there a way to ?= and override default?
# 2020-06-26 AMR NOTE: riscv64-unknown-elf-cc isn't installed (c++ probably not either)
CC = $(TARGET)-gcc
CXX = $(TARGET)-g++
CPP = $(TARGET)-cpp
AR = $(TARGET)-ar
HOSTCC ?= cc
HOSTCXX ?= c++

OBJCOPY ?= $(TARGET)-objcopy
STRIP ?= $(TARGET)-strip

DIR_GUARD=test -d $(@D) || mkdir -p $(@D)

# 2020-02-11 AMR TODO: naming (this isn't a rule)
define OBJ-rule
$1_OBJS = $$(foreach src,$$($1_SRCS),$$(patsubst %,$$(BUILD)/$2/%.o,$$(src)))
$1_DEPS = $$(foreach src,$$($1_SRCS),$$(patsubst %,$$(BUILD)/$2/%.dep,$$(src)))
endef

# 2020-02-12 AMR TODO: would a CC-ext-rule be easier?
define CC-rule
$$(BUILD)/$1/%.c.o: $2/%.c $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(CC) $3 -o $$@ -c $$<
$$(BUILD)/$1/%.c.dep: $2/%.c $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(CC) $3 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

define CXX-rule
$$(BUILD)/$1/%.cc.o: $2/%.cc $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(CXX) $3 -o $$@ -c $$<
$$(BUILD)/$1/%.cc.dep: $2/%.cc $3/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(CXX) $3 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

define HOSTCC-rule
$$(BUILD)/$1/%.c.o: $2/%.c $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(HOSTCC) $3 -o $$@ -c $$<
$$(BUILD)/$1/%.c.dep: $2/%.c $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(HOSTCC) $3 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

define HOSTCXX-rule
$$(BUILD)/$1/%.cc.o: $2/%.cc $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(HOSTCXX) $3 -o $$@ -c $$<
$$(BUILD)/$1/%.cc.dep: $2/%.cc $1/build.mk $$(SRCDIR)Makefile
		$$(DIR_GUARD)
		$$(HOSTCXX) $3 -o $$@ -MM -MT $$(patsubst %.dep,%.o,$$@) $$<
endef

$(foreach module,$(PROJECTS),$(eval include $(SRCDIR)$(module)/build.mk))

clean: $(foreach module,$(PROJECTS),clean-$(module))

depclean:
	test -f $(BUILD)/.keep || touch $(BUILD)/.keep
	find $(BUILD)/ -name '*.dep' | xargs $(RM)
	find $(BUILD)/ -type d -empty -delete

.PHONY: default $(PROJECTS) clean depclean deps
