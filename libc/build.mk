libc_DIR = $(SRCDIR)libc
pdclib_DIR = $(SRCDIR)ext/pdclib

# 2020-02-12 AMR NOTE: using exmaple platform for some things for now
pdclib_INCLUDES = \
	-I$(pdclib_DIR)/include
pdclib_CFLAGS = $(pdclib_INCLUDES) -D_PDCLIB_BUILD -D_PDCLIB_STATIC_DEFINE

libc_INCLUDES = \
	$(pdclib_INCLUDES) \
	-I$(libc_DIR)/include
libc_CFLAGS = $(libc_INCLUDES) $(pdclib_CFLAGS) $(CFLAGS)

pdclib_C_SRCS = \
	_PDCLIB/assert.c \
	_PDCLIB/errno.c \
	_PDCLIB/_PDCLIB_atomax.c \
	_PDCLIB/_PDCLIB_closeall.c \
	_PDCLIB/_PDCLIB_filemode.c \
	_PDCLIB/_PDCLIB_getstream.c \
	_PDCLIB/_PDCLIB_is_leap.c \
	_PDCLIB/_PDCLIB_isstream.c \
	_PDCLIB/_PDCLIB_load_lc_collate.c \
	_PDCLIB/_PDCLIB_load_lc_messages.c \
	_PDCLIB/_PDCLIB_load_lc_monetary.c \
	_PDCLIB/_PDCLIB_load_lc_numeric.c \
	_PDCLIB/_PDCLIB_load_lc_time.c \
	_PDCLIB/_PDCLIB_load_lines.c \
	_PDCLIB/_PDCLIB_prepread.c \
	_PDCLIB/_PDCLIB_prepwrite.c \
	_PDCLIB/_PDCLIB_seed.c \
	_PDCLIB/_PDCLIB_strtok.c \
	_PDCLIB/_PDCLIB_strtox_main.c \
	_PDCLIB/_PDCLIB_strtox_prelim.c \
	ctype/isalnum.c \
	ctype/isalpha.c \
	ctype/isblank.c \
	ctype/iscntrl.c \
	ctype/isdigit.c \
	ctype/isgraph.c \
	ctype/islower.c \
	ctype/isprint.c \
	ctype/ispunct.c \
	ctype/isspace.c \
	ctype/isupper.c \
	ctype/isxdigit.c \
	ctype/tolower.c \
	ctype/toupper.c \
	locale/localeconv.c \
	stdio/clearerr.c \
	stdio/fclose.c \
	stdio/feof.c \
	stdio/ferror.c \
	stdio/fflush.c \
	stdio/fgetc.c \
	stdio/fgetpos.c \
	stdio/fgets.c \
	stdio/fopen.c \
	stdio/fprintf.c \
	stdio/fputc.c \
	stdio/fputs.c \
	stdio/fread.c \
	stdio/freopen.c \
	stdio/fscanf.c \
	stdio/fseek.c \
	stdio/fsetpos.c \
	stdio/ftell.c \
	stdio/getc.c \
	stdio/getchar.c \
	stdio/perror.c \
	stdio/printf.c \
	stdio/putc.c \
	stdio/putchar.c \
	stdio/puts.c \
	stdio/rename.c \
	stdio/rewind.c \
	stdio/scanf.c \
	stdio/setbuf.c \
	stdio/setvbuf.c \
	stdio/snprintf.c \
	stdio/sscanf.c \
	stdio/tmpnam.c \
	stdio/ungetc.c \
	stdio/vfprintf.c \
	stdio/vfscanf.c \
	stdio/vprintf.c \
	stdio/vscanf.c \
	stdio/vsnprintf.c \
	stdio/vsscanf.c \
	string/memchr.c \
	string/memcmp.c \
	string/memcpy.c \
	string/memcpy_s.c \
	string/memmove.c \
	string/memmove_s.c \
	string/memset.c \
	string/memset_s.c \
	string/strcat.c \
	string/strcat_s.c \
	string/strchr.c \
	string/strcmp.c \
	string/strcoll.c \
	string/strcpy.c \
	string/strcpy_s.c \
	string/strcspn.c \
	string/strerror.c \
	string/strerrorlen_s.c \
	string/strerror_s.c \
	string/strlen.c \
	string/strncat.c \
	string/strncat_s.c \
	string/strncmp.c \
	string/strncpy.c \
	string/strncpy_s.c \
	string/strpbrk.c \
	string/strrchr.c \
	string/strspn.c \
	string/strstr.c \
	string/strtok.c \
	string/strtok_s.c \
	string/strxfrm.c \
	time/difftime.c
#	_PDCLIB/_PDCLIB_load_lc_ctype.c \
	_PDCLIB/_PDCLIB_scan.c \
	_PDCLIB/_PDCLIB_print.c \
	inttypes/imaxabs.c \
	inttypes/imaxdiv.c \
	inttypes/strtoimax.c \
	inttypes/strtoumax.c \
	locale/setlocale.c \
	stdio/fwrite.c \
	stdio/remove.c \
	stdio/sprintf.c \
	stdio/vsprintf.c \
	time/asctime.c \
	time/ctime.c \
	time/gmtime.c \
	time/localtime.c \
	time/mktime.c \
	time/strftime.c

# 2020-02-12 AMR FIXME: these taint the system
pdclib_plat_C_SRCS =
#	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_open.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_close.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_Exit.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_fillbuffer.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_flushbuffer.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_remove.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_rename.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_seek.c \
	$(pdclib_DIR)/platform/example/functions/_PDCLIB/_PDCLIB_stdinit.c \
	$(pdclib_DIR)/platform/example/functions/signal/raise.c \
	$(pdclib_DIR)/platform/example/functions/signal/signal.c \
	$(pdclib_DIR)/platform/example/functions/stdio/tmpfile.c \
	$(pdclib_DIR)/platform/example/functions/stdlib/getenv.c \
	$(pdclib_DIR)/platform/example/functions/stdlib/getenv_s.c \
	$(pdclib_DIR)/platform/example/functions/stdlib/system.c \
	$(pdclib_DIR)/platform/example/functions/threads/call_once.c \
	$(pdclib_DIR)/platform/example/functions/threads/cnd_broadcast.c \
	$(pdclib_DIR)/platform/example/functions/threads/cnd_destroy.c \
	$(pdclib_DIR)/platform/example/functions/threads/cnd_init.c \
	$(pdclib_DIR)/platform/example/functions/threads/cnd_signal.c \
	$(pdclib_DIR)/platform/example/functions/threads/cnd_timedwait.c \
	$(pdclib_DIR)/platform/example/functions/threads/cnd_wait.c \
	$(pdclib_DIR)/platform/example/functions/threads/mtx_destroy.c \
	$(pdclib_DIR)/platform/example/functions/threads/mtx_init.c \
	$(pdclib_DIR)/platform/example/functions/threads/mtx_lock.c \
	$(pdclib_DIR)/platform/example/functions/threads/mtx_timedlock.c \
	$(pdclib_DIR)/platform/example/functions/threads/mtx_trylock.c \
	$(pdclib_DIR)/platform/example/functions/threads/mtx_unlock.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_create.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_current.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_detach.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_equal.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_exit.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_join.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_sleep.c \
	$(pdclib_DIR)/platform/example/functions/threads/thrd_yield.c \
	$(pdclib_DIR)/platform/example/functions/threads/tss_create.c \
	$(pdclib_DIR)/platform/example/functions/threads/tss_delete.c \
	$(pdclib_DIR)/platform/example/functions/threads/tss_get.c \
	$(pdclib_DIR)/platform/example/functions/threads/tss_set.c \
	$(pdclib_DIR)/platform/example/functions/time/clock.c \
	$(pdclib_DIR)/platform/example/functions/time/time.c \
	$(pdclib_DIR)/platform/example/functions/time/timespec_get.c

libc_C_SRCS = \
	$(pdclib_C_SRCS) \
	$(pdclib_plat_C_SRCS)

$(eval $(call OBJ-rule,libc_C,$(libc_DIR)))
$(eval $(call CC-rule,$(libc_DIR),$(pdclib_DIR)/functions,$(libc_CFLAGS)))

$(BUILD)/libc.a: $(libc_C_OBJS)
	$(AR) rcs $@ $^

libc: $(BUILD)/libc.a

clean-libc:
	$(RM) $(wildcard $(libc_C_OBJS) $(BUILD)/libc.a)

.PHONY: clean-libc

-include $(libc_C_DEPS)
