libk_DIR = $(SRCDIR)libk
pdclib_DIR = $(SRCDIR)ext/pdclib

# 2020-02-12 AMR NOTE: using exmaple platform for some things for now
libk_pdclib_INCLUDES = \
	-I$(pdclib_DIR)/include
libk_pdclib_CFLAGS = $(pdclib_INCLUDES) -D_PDCLIB_BUILD -D_PDCLIB_STATIC_DEFINE

libk_INCLUDES = \
	$(pdclib_INCLUDES) \
	-I$(libc_DIR)/include
libk_CFLAGS = $(libc_INCLUDES) $(pdclib_CFLAGS) $(CFLAGS)

pdclib_C_SRCS =
pdclib_plat_C_SRCS =

libk_C_SRCS = \
	$(pdclib_C_SRCS) \
	$(pdclib_plat_C_SRCS)

$(eval $(call OBJ-rule,libk_C,$(libk_DIR)))
$(eval $(call CC-rule,$(libk_DIR),$(libk_pdclib_DIR)/functions,$(libk_CFLAGS)))

$(BUILD)/libk.a: $(libk_C_OBJS)
	$(AR) rcs $@ $^

libk: $(BUILD)/libk.a

clean-libk:
	$(RM) $(wildcard $(libk_C_OBJS) $(BUILD)/libk.a)

.PHONY: clean-libk

-include $(libk_C_DEPS)
