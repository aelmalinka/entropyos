test_DIR = $(SRCDIR)test

# 2020-06-26 AMR TODO: test pdclib (built in tests?)
# 2020-02-11 AMR TODO: figure out why -Wno-error isn't working
# 2020-02-11 AMR TODO: maybe fix gtest/gmock upstream?
gtest_CXXFLAGS = $(CXXFLAGS) \
	-Wno-error=useless-cast \
	-Wno-error=switch-default \
	-Wno-error=inline \
	-Wno-error=zero-as-null-pointer-constant \
	-Wno-error=missing-declarations \
	-Wno-useless-cast \
	-Wno-switch-default \
	-Wno-inline \
	-Wno-zero-as-null-pointer-constant \
	-Wno-missing-declarations
test_LDFLAGS = $(LDFLAGS) -L$(BUILD)/gtest/lib -lpthread
test_LIBS = -lgmock_main -lgmock -lgtest

test_CC_SRCS =

$(eval $(call OBJ-rule,test_CC,$(test_DIR)))
# 2020-02-12 AMR TODO: there has to be a way to cleanup the simple case
$(eval $(call HOSTCXX-rule,$(test_DIR),$(test_DIR),$(test_CXXFLAGS)))

test: $(BUILD)/test/all
	$(BUILD)/test/all

check: test

gtest: $(BUILD)/gtest/Makefile
	$(MAKE) -C $(BUILD)/gtest

$(BUILD)/gtest/Makefile: $(SRCDIR)ext/gtest
	test -d $(BUILD)/gtest || mkdir -p $(BUILD)/gtest
	( dir=`pwd` ; \
		cd $$(readlink -f $(BUILD)/gtest) ; \
		CXXFLAGS="$(gtest_CXXFLAGS)" cmake -DCMAKE_VERBOSE_MAKEFILE=On $$dir/ext/gtest )

clean-test:
	$(RM) $(wildcard $(test_CC_OBJS) $(BUILD)/test/all)
	$(RM) -r $(BUILD)/gtest

$(BUILD)/test/all: gtest $(test_CC_OBJS)
	$(DIR_GUARD)
	$(HOSTCXX) -o $@ $(test_LDFLAGS) $(test_LIBS) $(test_CC_OBJS)

.PHONY: gtest check test clean-test

-include $(test_CC_DEPS)
